

## About Laravel

This is a CHAMA laravel Api with the functionality of members lending loans and saving.
Laravel repository design pattern was used in development

## Installation

1. Clone the project to your machine. 

```
$ git clone https://gitlab.com/kibetbrian/mojagate-sms.git

```  

2. Install composer packages

```
$ composer install OR 
$ composer update

```  
3. Create .env file
4. Create your database
5. Migrate your database 

```
$ php artisan migrate

``` 


6. Start laravel local server  then test your Api's

```
$ php artisan serve

``` 

## Contact

- Name: Brian Kiptanui.
- Email: kimutaibrian9@gmail.com

## Acknowledgments

Mojagate developers
