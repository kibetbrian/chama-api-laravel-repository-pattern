<?php

namespace App\Models;

use App\Traits\SearchableTrait;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends BaseModel
{
    use Notifiable, SearchableTrait, HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customers';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'id';

    protected $dates = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'phone',
        'email',
        'country',
        'city',
        'company_id',
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'customers.first_name' => 1,
            'customers.last_name' => 1,
            'customers.email' => 1,
        ]
    ];
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
    public function invoive()
    {
        return $this->hasMany(Invoice::class, 'customer_id');
    }
    public function payment()
    {
        return $this->hasMany(Payment::class, 'invoice_number_id', 'id');
    }
    public function quotation()
    {
        return $this->hasMany(Quotation::class, 'customer_id');
    }
}
