<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\InvoiceRequest;
use App\Http\Resources\InvoiceResource;
use App\Invoicer\Repositories\Contracts\InvoiceInterface;
use Illuminate\Http\Request;

class InvoiceController extends ApiController
{
    protected $invoiceRepository, $load;

    public function __construct(InvoiceInterface $invoiceInterface)
    {
        $this->invoiceRepository = $invoiceInterface;
        $this->load = [
            'payment',
            'company',
            'customer',
            'product'
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($select = request()->query('list')) {
            return $this->invoiceRepository->listAll($this->formatFields($select), []);
        } else
            $data = InvoiceResource::collection($this->invoiceRepository->getAllPaginate($this->load));
        return $this->respondWithData($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InvoiceRequest $request)
    {
        $data = $request->all();
        $save = $this->invoiceRepository->create($data);

        if (!is_null($save) && $save['error']) {
            return $this->respondNotSaved($save['message']);
        } else {
            return $this->respondWithSuccess('Success !! Invoice has been created.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $invoice = $this->invoiceRepository->getById($uuid);

        if (!$invoice) {
            return $this->respondNotFound('Invoice not found.');
        }
        return $this->respondWithData(new InvoiceResource($invoice));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InvoiceRequest $request, $uuid)
    {
        $save = $this->invoiceRepository->update(array_filter($request->all()), $uuid);

        if (!is_null($save) && $save['error']) {
            return $this->respondNotSaved($save['message']);
        } else {
            return $this->respondWithSuccess('Success !! Invoice has been updated.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        if ($this->invoiceRepository->delete($uuid)) {
            return $this->respondWithSuccess('Success !! Invoice has been deleted');
        }
        return $this->respondNotFound('Invoice Category not deleted');
    }
}
